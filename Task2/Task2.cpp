#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
/*
* �������� 2
���� ����� � ����������� ������� �������-���������� ��
�������. ���������:
1) ���������, �� � � ����� ����� ���������;
2) ��������� ������� ��������� ������� �����;
3) ������ �����, �� ������������ � ����� ����������� ������� ����;
4) �������� �� ����� ���������� ���, ���������� ����� �� ����� ���;
5) ����������� ����� � ���������� �������
*/
void putElement(char*** keys,int** values,int* size,char* key, int value) {
	for (int i = 0; i < *size; i++) {
		if (strcmp((* keys)[i], key) == 0) {
			(* values)[i] = value;
			return;
		}
	}
	(*size)++;
	*keys = (char**)realloc(*keys, *size * sizeof(char*));
	*values = (int*)realloc(*values, *size * sizeof(int));
	(* keys)[*size - 1] = key;
	(* values)[*size - 1] = value;
}
int getElement(char** keys, int* values, int* size, char* key) {
	for (int i = 0; i < *size; i++) {
		if (strcmp(keys[i], key) == 0) {
			return values[i];
		}
	}
	return 0;
}
int main() {
	int size = 0;
	char s[256];
	char** words = (char**)malloc(sizeof(char*));
	int* count = (int*)malloc(sizeof(int));
	
	words[0] = (char*)malloc(sizeof(char));
	strcpy(words[0], "");
	count[0] = 0;

	bool palindromes = false;

	puts("Enter string:");
	gets_s(s);
	char* word = strtok(s, " ");
	while (word != NULL) {
		int value = getElement(words, count, &size, word);
		putElement(&words,&count, &size, word, value + 1);
		char s2[256];
		strcpy(s2, word);
		_strrev(s2);
		if (strcmp(s2, word) == 0) {
			palindromes = true;
		}
		word = strtok(NULL, " ");
	}
	int max=-1, max_i=-1;
	printf(palindromes ? "String has palindromes" : "There are no palindromes in string");
	printf("\n\n");
	for (int i = 0; i < size; i++) {
		printf("Word %s repeats %d times\n", words[i],count[i]);
		if (count[i] > max) {
			max = count[i];
			max_i = i;
		}
	}
	char s2[256]={0};
	for (int i = 0; i < size; i++) {
		strcat(s2, words[i]);
		strcat(s2, " ");
	}
	printf("New string without repeats:\n%s\n",s2);
	for (int i = 0; i < size; i++) {
		bool swaps = false;
		for (int j = 0; j < size-1; j++) {
			int La = strlen(words[j]);
			int Lb = strlen(words[j+1]);
			for (int k = 0; k < (La > Lb ? Lb : La) + 1; k++) {
				char ca = tolower(words[j][k]);
				char cb = tolower(words[j+1][k]);
				if (ca > cb) {
					char* p = words[j];
					words[j] = words[j + 1];
					words[j + 1] = p;
					swaps = true;
					break;
				}
			}
		}
		if (!swaps)
			break;
	}
	char s3[256] = {0};
	for (int i = 0; i < size; i++) {
		strcat(s3, words[i]);
		strcat(s3, " ");
	}
	printf("Sorted string:\n%s\n", s3);
	free(words);
	free(count);
	return 0;
}